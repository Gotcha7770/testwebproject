using NUnit.Framework;
using TestWebApplication.Utils;

namespace Tests
{
    public class IntExtensionsTests
    {
        [Test]
        public void EvenOddTest()
        {
            Assert.IsTrue(4.IsEven());
            Assert.IsTrue(12.IsEven());

            Assert.IsFalse(1.IsEven());
            Assert.IsFalse(11.IsEven());
        }
    }
}