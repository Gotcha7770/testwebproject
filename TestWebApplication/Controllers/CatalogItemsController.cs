﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TestWebApplication.Data;
using TestWebApplication.Interfaces;
using TestWebApplication.Models;
using TestWebApplication.Utils;

namespace TestWebApplication.Controllers
{
    
    public abstract class CatalogItemsController<T> : ControllerBase
        where T : class, ICatalogItem
    {
        private readonly ApplicationContext _applicationContext;

        protected CatalogItemsController(ApplicationContext applicationContext)
        {
            _applicationContext = applicationContext;
        }

        [HttpGet]
        public virtual async Task<JsonResult> All(FilterState filter = FilterState.None, SortState sort = SortState.None)
        {
            IQueryable<T> items;

            switch (filter)
            {
                case FilterState.EvenNameLength:
                    items = _applicationContext.Set<T>().Where(x => x.Name.Length.IsEven());
                    break;
                default:
                    items = _applicationContext.Set<T>();
                    break;
            }

            switch (sort)
            {
                case SortState.CountAsc:
                    items = items.OrderBy(x => x.Count);
                    break;
                case SortState.CountDesc:
                    items = items.OrderByDescending(x => x.Count);
                    break;
            }
            
            return new JsonResult(await items.AsNoTracking().ToListAsync());
        }

        [HttpGet]
        public async Task<JsonResult> Items(long? id, string name, int? count)
        {
            IQueryable<T> items = _applicationContext.Set<T>();

            if (id.HasValue && id != 0)
                items = items.Where(x => x.Id == id);

            if (!string.IsNullOrEmpty(name))
                items = items.Where(x => x.Name == name);

            if (count.HasValue && count != 0)
                items = items.Where(x => x.Count == count);

            return new JsonResult(await items.AsNoTracking().ToListAsync());
        }
    }

    [Route("catalog/[controller]")]
    [ApiController]
    public class VacuumCleanersController : CatalogItemsController<VacuumCleaner>
    {
        public VacuumCleanersController(ApplicationContext applicationContext) 
            : base(applicationContext)
        { }
    }

    [Route("catalog/[controller]")]
    [ApiController]
    public class PiecesOfArtController : CatalogItemsController<PieceOfArt>
    {
        public PiecesOfArtController(ApplicationContext applicationContext)
            : base(applicationContext)
        { }
    }

    [Route("catalog/[controller]")]
    [ApiController]
    public class BeautyProductsController : CatalogItemsController<BeautyProduct>
    {
        public BeautyProductsController(ApplicationContext applicationContext)
            : base(applicationContext)
        { }
    }
}
