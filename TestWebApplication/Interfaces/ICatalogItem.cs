﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestWebApplication.Interfaces
{
    public interface ICatalogItem
    {
        long Id { get; }

        string Name { get; }

        int Count { get; }
    }
}
