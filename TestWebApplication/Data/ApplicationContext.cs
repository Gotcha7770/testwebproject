﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TestWebApplication.Models;

namespace TestWebApplication.Data
{
    public enum SortState
    {
        None,
        CountAsc,
        CountDesc,
    }

    public enum FilterState
    {
        None,
        EvenNameLength
    }

    public class ApplicationContext : DbContext
    {
        public DbSet<VacuumCleaner> VacuumCleaners { get; set; }

        public DbSet<PieceOfArt> PiecesOfArt { get; set; }

        public DbSet<BeautyProduct> BeautyProducts { get; set; }

        public ApplicationContext()
        {
            Database.EnsureCreated();
        }
    }
}
