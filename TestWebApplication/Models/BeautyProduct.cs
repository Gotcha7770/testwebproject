﻿using TestWebApplication.Interfaces;

namespace TestWebApplication.Models
{
    public class BeautyProduct : ICatalogItem
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public int Count { get; set; }
    }
}
